CONTENTS OF THIS FILE
---------------------

 * Introduction
 * How to use this module ?


 INTRODUCITON
-----------------
This module is built to use with the ckeditor. If this module configured properly.
It adds a line of color, chosen by the user on above of the content.


How to use this module?
------------------------

To use this module you have to go to configuration tab to your drupal 8 admin page.
And then content authoring->text formats and editors->Full Html->configuration->and then add 'active page break' to the active tool of ckeditor.


